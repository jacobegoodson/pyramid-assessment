/*
    PROBLEM STATEMENT
        In this wonderful method below you will have to find the two highest
        integer elements within the passed in array. Once you dig in and find the
        two highest elements you will then need to sum them up and return the max
        of the two highest values in the array!

    EXAMPLE
        sumArrayElementsMax([1, 2, 3, 3]) => 6
        sumArrayElementsMax([5, 6, 7, 8, 9]) => 17
 */
package test_questions;

public class SumArrayElementsMax {

    public static int sumArrayElementsMax(int[] nums) {
       //Your code goes here
        return 1;
    }
}
