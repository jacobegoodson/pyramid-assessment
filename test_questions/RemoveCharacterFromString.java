/*
    PROBLEM STATEMENT
        In a galaxy far far away the jedi are wanting a new Padawan to test his abilities
        with the force. So, get ready you are tasked by Yoda to complete the method below
        and remove the unwanted character from the string.

    EXAMPLE
        removeCharacter("Luke", 'k') => "Lue"
        removeCharacter("Skywalker" , 'w') => "Skyalker"
        removeCharacter("Darth Vader", 'a') => "Drth Vder"
*/
package test_questions;

public class RemoveCharacterFromString {

    public static String removeCharacter(String word, char unwanted){
        //Your code goes here
        return null;
    }
}
