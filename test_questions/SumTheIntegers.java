/*
    PROBLEM STATEMENT
        Complete the method to sum all the integers in a integer. The method will take in an integer
        and then you will need to sum all the digits in the integer and the return the sum.

    EXAMPLE
        sumTheIntegers(12345) => 15
        sumTheIntegers(123) => 6

    DISALLOWED TECHNIQUES
        Your are not allowed to use streams

 */
package test_questions;

public class SumTheIntegers {

    public static int sumIntegers(int number){
        // Your code goes here
        return 0;
    }
}
