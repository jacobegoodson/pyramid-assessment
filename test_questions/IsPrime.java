package test_questions;

/*
    PROBLEM STATEMENT
        A prime number, n, is a number that is positive and divisible only by 1 and n
        Implement isPrime to calculate whether a number is prime or not.

    EXAMPLE
        isPrime(2) => true
        isPrime(8) => false
        isPrime(11) => true
        isPrime(24) => false

*/

public class IsPrime {
    public static boolean isPrime(int n) {
        // your code goes here
        return false;
    }
}
