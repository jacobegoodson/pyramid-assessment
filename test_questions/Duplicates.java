package test_questions;

/*
    PROBLEM STATEMENT
        Complete the method below by detecting all the duplicates in the input
        array and outputting those duplicates in another array. No duplicates should
        be repeated in the output array.

    IMPORTANT
        The array that is returned must contain the duplicates in the order that they
        are found

    Example
        duplicates([1 2 3 1]) => [1]
        duplicates([1 2 3]) => []
        duplicates([1 2 3 1 1 1 1 1]) => [1]
        duplicates([1 1 1 2 2 2 2 2 2 2 3]) => [1 2]

    DISALLOWED TECHNIQUES
        Your are not allowed to use streams
*/

public class Duplicates {
    public static int[] duplicates(int[] toArray) {
        return new int[0];
    }
}
