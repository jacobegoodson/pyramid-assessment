package test_questions;

/*
    PROBLEM STATEMENT
        Complete the method below by returning true if the input is
        false and by returning false if the input is true

    EXAMPLE
        isOpposite(false) => true
        isOpposite(true) => false

*/

public class OppositeDay {
    public static boolean isOpposite(boolean b) {
        return false;
    }
}
