/*
    PROBLEM STATEMENT:
        You are required to reverse the string word,
        and then return the reversed string, word.

    EXAMPLE:
        reverseString("hello") => "olleh"
        reverseString("123") => "321"

    DISALLOWED TECHNIQUES
        The java method .reverse is not allowed
*/
package test_questions;

public class ReverseAString {

    public static String reverseString(String word){
        //Your code goes here
        return null;
    }
}
