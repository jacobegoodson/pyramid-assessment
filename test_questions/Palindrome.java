/*
    PROBLEM STATEMENT:
        In this problem you will need to check to see if a string is a palindrome!
        A palindrome is when the reversed string is equal to the original string.
        You will need to return "pal" if it is a palindrome and "non-pal" if it is
        not a palindrome.

    EXAMPLE:
        checkForPal("mam") => "pal"
        checkForPal("heaven") => "non-pal"
        checkForPal("") => ""
*/
package test_questions;

public class Palindrome {

    public static String checkForPal(String s){
        //Your code goes here
        return null;
    }
}
