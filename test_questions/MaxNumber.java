/*
    PROBLEM STATEMENT
        Please complete the method below for the village people at the corner lake site.
        They have been experiencing problems with a simple problem of finding the max
        number of of fish to give to the clients. Please complete the method below
        that will take in two integers and it will need to return the max of the
        two numbers!

    EXAMPLE
        maxNumTest(2, 4) => 4
        maxNumTest(6, 3) => 6

    DISALLOWED TECHNIQUES
        Your are not allowed to Math.max
 */

package test_questions;

public class MaxNumber {

    public static int maxNumTest(int firstNumber, int secondNumber) {
        return 0;
    }
}
