package test_questions;

/*
    PROBLEM STATEMENT
        Complete the method below by reversing n, the input to the method

    Example
        reverseAnInteger(123) => 321
        reverseAnInteger(1) => 1
        reverseAnInteger(8778) => 8778
        reverseAnInteger(344) => 443

    DISALLOWED TECHNIQUES
        Your are not allowed to use .reverse in your solution

*/

public class ReverseAnInteger {
    public static int reverseAnInteger(int n) {
        // your code goes here
        return 0;
    }
}
