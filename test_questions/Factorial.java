package test_questions;

/*
    PROBLEM STATEMENT:
        You are required to produce the factorial of a given n.
        The factorial definition is as follows...
        n! = 1*2*3 ...n = n... 3*2*1

    EXAMPLE:
        factorial(3) => 6 because 3 * 2 * 1 = 6
        factorial(1) => 1
        factorial(6) => 780 because 6 * 5 * 4 * 3 * 2 * 1 = 780

    DISALLOWED TECHNIQUES
        You are not allowed to use streams for this, you must
        provide an iterative solution, or a recursive one
*/

public class Factorial {
    public static int factorial(int n){
        // your code goes here
        return 0;
    }
}
