import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test_questions.EvenOrOdd;

import java.util.ArrayList;
import java.util.List;

public class EvenOrOddTest {

    @Property void evenOrOddTestForEvens(@ForAll("genEvens") int n){
        Assertions.assertEquals("even", EvenOrOdd.checkForEvenOrOdd(n), TestHelper.message(n));
    }

    @Property void evenOrOddTestForOdds(@ForAll("getOdd") int n){
        Assertions.assertEquals("odd", EvenOrOdd.checkForEvenOrOdd(n), TestHelper.message(n));
    }

    @Provide
    Arbitrary<Integer> genEvens(){
        List<Integer> list = new ArrayList<Integer>();
        for(int i = 2; i <= 1000; i+=2){
            list.add(i);
        }
        return Arbitraries.of(list);
    }

    @Provide
    Arbitrary<Integer> getOdd(){
        List<Integer> list1 = new ArrayList<Integer>();
        for(int i = 1; i <= 1001; i+=2){
            list1.add(i);
        }
        return Arbitraries.of(list1);
    }

}
