import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test_questions.SumArrayElementsMax;

public class SumArrayElementsMaxTest {

    @Test void sumArrayElementsMaxTest1(){
        Assertions.assertEquals(250, SumArrayElementsMax.sumArrayElementsMax(new int[] {5,25,125,125}));
    }

    @Test void sumArrayElementsMaxTest2(){
        Assertions.assertEquals(17,SumArrayElementsMax.sumArrayElementsMax(new int[] {5,6,7,8,9}));
    }

    @Test void sumArrayElementsMaxTest3(){
        Assertions.assertEquals(1800, SumArrayElementsMax.sumArrayElementsMax(new int[] {10,800,500,1000}));
    }

    @Test void sumArrayElementMaxTest4(){
        Assertions.assertEquals(9378, SumArrayElementsMax.sumArrayElementsMax(new int[] {1058,2589,6789,1,5}));
    }

    @Test void sumArrayElementsMaxTest5(){
        Assertions.assertEquals(1244,SumArrayElementsMax.sumArrayElementsMax(new int[] {555,689,25,65}));
    }
    @Test void sumArrayElementsMaxTest6(){
        Assertions.assertEquals(1244,SumArrayElementsMax.sumArrayElementsMax(new int[] {555,689,25,65}));
    }
    @Test void sumArrayElementsMaxTest7(){
        Assertions.assertEquals(1244,SumArrayElementsMax.sumArrayElementsMax(new int[] {555,689,25,65}));
    }
    @Test void sumArrayElementsMaxTest8(){
        Assertions.assertEquals(1244,SumArrayElementsMax.sumArrayElementsMax(new int[] {555,689,25,65}));
    }
    @Test void sumArrayElementsMaxTest9(){
        Assertions.assertEquals(1244,SumArrayElementsMax.sumArrayElementsMax(new int[] {555,689,25,65}));
    }
    @Test void sumArrayElementsMaxTest10(){
        Assertions.assertEquals(1244,SumArrayElementsMax.sumArrayElementsMax(new int[] {555,689,25,65}));
    }
}
