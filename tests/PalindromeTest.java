import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test_questions.Palindrome;

public class PalindromeTest {

    @Test void palindromeTest(){
        Assertions.assertEquals("pal", Palindrome.checkForPal("mam"));
    }

    @Test void palindromeTest1(){
        Assertions.assertEquals("non-pal", Palindrome.checkForPal("Hello"));
    }

    @Test void palindromeTest2(){
        Assertions.assertEquals("pal", Palindrome.checkForPal("dood"));
    }

    @Test void palindromeTest3(){
        Assertions.assertEquals("non-pal", Palindrome.checkForPal("Bakery"));
    }

    @Test void palindromeTest4(){
        Assertions.assertEquals("pal", Palindrome.checkForPal("racecar"));
    }

    @Test void palindromeTest5(){
        Assertions.assertEquals("pal", Palindrome.checkForPal("a"));
    }

    @Test void palindromeTest6(){
        Assertions.assertEquals("", Palindrome.checkForPal(""));
    }
}
