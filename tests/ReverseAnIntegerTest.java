import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;
import test_questions.ReverseAnInteger;

public class ReverseAnIntegerTest {
    @Property void reverseAnIntegerTest0(@ForAll("gen_nums") int n){
        Assertions.assertEquals(testMethod(n), ReverseAnInteger.reverseAnInteger(n), TestHelper.message(n));
    }
    @Property void reverseAnIntegerTest1(@ForAll("gen_nums") int n){
        Assertions.assertEquals(testMethod(n), ReverseAnInteger.reverseAnInteger(n), TestHelper.message(n));
    }
    @Property void reverseAnIntegerTest2(@ForAll("gen_nums") int n){
        Assertions.assertEquals(testMethod(n), ReverseAnInteger.reverseAnInteger(n), TestHelper.message(n));
    }
    @Property void reverseAnIntegerTest3(@ForAll("gen_nums") int n){
        Assertions.assertEquals(testMethod(n), ReverseAnInteger.reverseAnInteger(n), TestHelper.message(n));
    }
    @Property void reverseAnIntegerTest4(@ForAll("gen_nums") int n){
        Assertions.assertEquals(testMethod(n), ReverseAnInteger.reverseAnInteger(n), TestHelper.message(n));
    }
    @Property void reverseAnIntegerTest5(@ForAll("gen_nums") int n){
        Assertions.assertEquals(testMethod(n), ReverseAnInteger.reverseAnInteger(n), TestHelper.message(n));
    }
    @Property void reverseAnIntegerTest6(@ForAll("gen_nums") int n){
        Assertions.assertEquals(testMethod(n), ReverseAnInteger.reverseAnInteger(n), TestHelper.message(n));
    }
    @Property void reverseAnIntegerTest7(@ForAll("gen_nums") int n){
        Assertions.assertEquals(testMethod(n), ReverseAnInteger.reverseAnInteger(n), TestHelper.message(n));
    }

    @Provide
    Arbitrary<Integer> gen_nums(){
        return Arbitraries.integers().between(1,10000);
    }

    // you are disallowed from using this solution!!!
    private int testMethod(int n){
        return Integer.parseInt(new StringBuilder(String.valueOf(n)).reverse().toString());
    }

}
