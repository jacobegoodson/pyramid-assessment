import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test_questions.HelloWorld;

public class HelloWorldTest {
    @Test
    void helloWorldTest(){
        Assertions.assertEquals("Hello World", HelloWorld.helloWorld());
    }
}
