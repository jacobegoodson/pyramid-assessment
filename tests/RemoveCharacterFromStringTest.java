import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test_questions.RemoveCharacterFromString;

public class RemoveCharacterFromStringTest {

    @Test void removeCharacterFromStringTest1(){
        Assertions.assertEquals("NINA", RemoveCharacterFromString.removeCharacter("NINJA", 'J'));
    }

    @Test void removeCharacterFromStringTest2(){
        Assertions.assertEquals("Call O Duty", RemoveCharacterFromString.removeCharacter("Call Of Duty", 'f'));
    }

    @Test void removeCharacterFromStringTest3(){
        Assertions.assertEquals("Draon", RemoveCharacterFromString.removeCharacter("Dragon",'g'));
    }

    @Test void removeCharacterFromStringTest4(){
        Assertions.assertEquals("jumnji", RemoveCharacterFromString.removeCharacter("jumanji", 'a'));
    }

    @Test void removeCharacterFromStringTest5(){
        Assertions.assertEquals("Fortite", RemoveCharacterFromString.removeCharacter("Fortnite", 'n'));
    }

    @Test void removeCharacterFromStringTest6(){
        Assertions.assertEquals("brcdbr", RemoveCharacterFromString.removeCharacter("abracadabra", 'a'));
    }
    @Test void removeCharacterFromStringTest7(){
        Assertions.assertEquals("brcdbr", RemoveCharacterFromString.removeCharacter("abracadabra", 'a'));
    }
    @Test void removeCharacterFromStringTest8(){
        Assertions.assertEquals("brcdbr", RemoveCharacterFromString.removeCharacter("abracadabra", 'a'));
    }
    @Test void removeCharacterFromStringTest9(){
        Assertions.assertEquals("brcdbr", RemoveCharacterFromString.removeCharacter("abracadabra", 'a'));
    }
}
