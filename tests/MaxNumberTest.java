import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test_questions.MaxNumber;

public class MaxNumberTest {

    @Test void maxNumberTest1(){
        Assertions.assertEquals(5, MaxNumber.maxNumTest(2, 5));
        Assertions.assertEquals(5, MaxNumber.maxNumTest(3, 5));
        Assertions.assertEquals(6, MaxNumber.maxNumTest(6, 5));
        Assertions.assertEquals(9, MaxNumber.maxNumTest(2, 9));
        Assertions.assertEquals(5, MaxNumber.maxNumTest(5, 5));
    }
}
