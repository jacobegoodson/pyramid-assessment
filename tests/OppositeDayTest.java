import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test_questions.OppositeDay;

public class OppositeDayTest {
    @Test
    void oppositeDayTest(){
        Assertions.assertFalse(OppositeDay.isOpposite(true));
        Assertions.assertTrue(OppositeDay.isOpposite(false));
    }
}
