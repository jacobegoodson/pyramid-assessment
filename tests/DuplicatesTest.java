import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import org.junit.jupiter.api.Assertions;
import test_questions.Duplicates;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DuplicatesTest {
    int[] findDups(List<Integer> dups){
        AtomicInteger index = new AtomicInteger();
        return dups.stream().filter(i -> {
            index.getAndIncrement();
            return dups.subList(index.get(), dups.size())
                    .stream()
                    .filter(j -> j.equals(i))
                    .toArray()
                    .length > 0;
        }).distinct().mapToInt(Integer::intValue).toArray();
    }

    @Property
    void duplicatesTest1(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest2(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest3(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest4(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest5(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest6(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest7(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest8(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest9(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest10(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest11(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest12(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest13(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest14(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }
    @Property
    void duplicatesTest15(@ForAll List<Integer> dups){
        Assertions.assertArrayEquals(
                findDups(dups),
                Duplicates.duplicates(dups.stream().mapToInt(Integer::intValue).toArray()),
                TestHelper.message(dups)
        );
    }


}
