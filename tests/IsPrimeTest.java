
import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;
import test_questions.IsPrime;

import java.util.ArrayList;
import java.util.Arrays;

public class IsPrimeTest {
    @Property
    void isPrimeTest0(@ForAll("prime_generator") int n){
        Assertions.assertTrue(IsPrime.isPrime(n), TestHelper.message(n));
    }
    @Property
    void isPrimeTest1(@ForAll("prime_generator") int n){
        Assertions.assertTrue(IsPrime.isPrime(n), TestHelper.message(n));
    }
    @Provide
    Arbitrary<Integer> prime_generator(){
        String strPrimeNums =
        "2 3 5 7 11 13 17 19 23 29 " +
        "31 37 41 43 47 53 59 61 67 71 " +
        "73 79 83 89 97 101 103 107 109 113 " +
        "127 131 149 151 157 163 167 173 " +
        "179 181 197 199 211 223 227 229 " +
        "233 239 257 263 269 271 277 281 " +
        "283 293 313 317 331 337 347 349 " +
        "353 359 379 383 389 397 401 409 " +
        "419 421 439 443 449 457 461 463 " +
        "467 479 499 503 509 521 523 541 " +
        "547 557 571 577 587 593 599 601 " +
        "607 613 631 641 643 647 653 659 " +
        "661 673 691 701 709 719 727 733 " +
        "739 743 761 769 773 787 797 809 " +
        "811 821 829 839 853 857 859 863 " +
        "877 881 907 911 919 929 937 941 " +
        "947 953 977 983 991 997 1009 1013 " +
        "1019 1021 1039 1049 1051 1061 1063 1069 " +
        "1087 1091 1103 1109 1117 1123 1129 1151 " +
        "1153 1163 1187 1193 1201 1213 1217 1223";
        int[] nums = Arrays.stream(strPrimeNums.split(" ")).mapToInt(Integer::parseInt).toArray();
        ArrayList<Integer> listNums = new ArrayList<>();
        for (int n : nums) {
            listNums.add(n);
        }
        return Arbitraries.of(listNums);

    }
}
