import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;
import test_questions.ReverseAString;

public class ReverseAStringTest {

    @Property void reverseAStringTest0(@ForAll("stringGen") String s){
        Assertions.assertEquals(reverse(s), ReverseAString.reverseString(s), TestHelper.message(s));
    }
    @Property void reverseAStringTest1(@ForAll("stringGen") String s){
        Assertions.assertEquals(reverse(s), ReverseAString.reverseString(s), TestHelper.message(s));
    }
    @Property void reverseAStringTest2(@ForAll("stringGen") String s){
        Assertions.assertEquals(reverse(s), ReverseAString.reverseString(s), TestHelper.message(s));
    }
    @Property void reverseAStringTest3(@ForAll("stringGen") String s){
        Assertions.assertEquals(reverse(s), ReverseAString.reverseString(s), TestHelper.message(s));
    }

    @Provide
    Arbitrary<String> stringGen(){

        return Arbitraries.strings();
    }

    //You are not allowed to use this code
    String reverse(String s){
        return new StringBuilder(s).reverse().toString();
    }
}
