import java.util.List;

public class TestHelper {
    public static String message(int n){
        return "Current input tested was " + n;
    }

    public static String message(String n){
        return "Current input tested was " + n;
    }

    public static String message(List<Integer> dups) {
        return "Current input tested was " + dups;
    }
}
