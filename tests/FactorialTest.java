import net.jqwik.api.*;
import net.jqwik.api.constraints.IntRange;
import org.junit.jupiter.api.Assertions;
import test_questions.Factorial;

import java.util.stream.IntStream;

public class FactorialTest {
    @Property void factorialTest0(@ForAll("gen_nums") int n){
        Assertions.assertEquals(
                IntStream.rangeClosed(1,n).reduce((prev,next) -> prev * next).getAsInt(),
                Factorial.factorial(n),
                TestHelper.message(n)
        );
    }
    @Property void factorialTest1(@ForAll("gen_nums") int n){
        Assertions.assertEquals(
                IntStream.rangeClosed(1,n).reduce((prev,next) -> prev * next).getAsInt(),
                Factorial.factorial(n),
                TestHelper.message(n)
        );
    }
    @Property void factorialTest2(@ForAll("gen_nums") int n){
        Assertions.assertEquals(
                IntStream.rangeClosed(1,n).reduce((prev,next) -> prev * next).getAsInt(),
                Factorial.factorial(n),
                TestHelper.message(n)
        );
    }
    @Provide
    Arbitrary<Integer> gen_nums(){
        return Arbitraries.integers().between(1,60);
    }
}
