import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test_questions.SumTheIntegers;

import java.util.Arrays;

public class SumTheIntegersTest {

    @Test void sumTheIntegersTest1(){
        Assertions.assertEquals(15, SumTheIntegers.sumIntegers(12345));
    }

    @Property void sumTheIntegersTest2(@ForAll("intGen") int n){
        Assertions.assertEquals(
                Arrays.stream(String.valueOf(n).split("")).mapToInt(Integer::parseInt).sum(),
                SumTheIntegers.sumIntegers(n),
                TestHelper.message(n)
        );
    }
    @Property void sumTheIntegersTest3(@ForAll("intGen") int n){
        Assertions.assertEquals(
                Arrays.stream(String.valueOf(n).split("")).mapToInt(Integer::parseInt).sum(),
                SumTheIntegers.sumIntegers(n),
                TestHelper.message(n)
        );
    }

    @Provide
    Arbitrary<Integer> intGen(){
       return Arbitraries.integers().between(0,20000);
    }
}
